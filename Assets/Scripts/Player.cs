﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public Vector2 gridPosition = Vector2.zero;
    public float moveSpeed = 10f;
    public Vector3 moveDestination;

    public int movementPerActionPoint = 5;
    public int attackRange = 1;

    public bool isMoving = false;
    public bool isAttacking = false;
    public int HP = 25;
    public float attackChance = 0.75f;
    public float defencereduction = 0.15f;
    public int baseDamage = 5;
    public float damageRollSide = 6;
    public string playerName = "Bot";

    public int numberOfActionsPerTurn = 2;

    //movement animation;
    public List<Vector3> positionQueue = new List<Vector3>();

    private void Awake()
    {
        moveDestination = transform.position;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       
    }

    /*
     * An abstract function cannot have functionality. 
     * You're basically saying, any child class MUST give their own version of this method, however it's too general to even try to implement in the parent class.

       A virtual function, is basically saying look, here's the functionality that may or may not be good enough for the child class. 
       So if it is good enough, use this method, if not, then override me, and provide your own functionality.
     */

    public virtual void TurnUpdate()
    {
        if (numberOfActionsPerTurn <= 0)
        {
            numberOfActionsPerTurn = 2;
            isMoving = false;
            isAttacking = false;
            GameManager.getInstance().NextTurn();
        }
    }

    public virtual void TurnOnGUI()
    {

    }
}
