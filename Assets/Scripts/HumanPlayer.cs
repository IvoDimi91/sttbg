﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanPlayer : Player {

    //public float moveSpeed = 10f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (GameManager.getInstance().players[GameManager.getInstance().currentPlayerIndex].Equals(this))
        {
            transform.GetComponent<Renderer>().material.color = Color.green;
        }
        else
        {
            transform.GetComponent<Renderer>().material.color = Color.white;
        }

        if(HP <= 0)
        {
            transform.rotation = Quaternion.Euler(new Vector3(90, 0, 0));
            transform.GetComponent<Renderer>().material.color = Color.red;
        }
    }

    public override void TurnUpdate()
    {

        if(positionQueue.Count > 0 )
        {
            if (Vector3.Distance(positionQueue[0], transform.position) > 0.1f)
            {
                transform.position += (positionQueue[0] - transform.position).normalized * moveSpeed * Time.deltaTime;

                if (Vector3.Distance(positionQueue[0], transform.position) <= 0.1f)
                {
                    transform.position = positionQueue[0];
                    positionQueue.RemoveAt(0);
                    if(positionQueue.Count == 0)
                    {
                        numberOfActionsPerTurn--;
                    }
                }
            }
        }

        //Highlight
        //if(Vector3.Distance(moveDestination, transform.position) > 0.1f)
        //{
        //    transform.position += (moveDestination - transform.position).normalized * moveSpeed * Time.deltaTime;

        //    if (Vector3.Distance(moveDestination, transform.position) <= 0.1f)
        //    {
        //        transform.position = moveDestination;
        //        numberOfActionsPerTurn--;
        //    }
        //}

        base.TurnUpdate();
    }

    public override void TurnOnGUI()
    {
        float buttonHeight = 50;
        float buttonWidth = 150;

        //move button
        Rect rectButton = new Rect(0, Screen.height - buttonHeight * 3, buttonWidth, buttonHeight);

        if (GUI.Button(rectButton, "Move"))
        {
            if(!isMoving)
            {
                GameManager.getInstance().RemoveTileHighlights();
                isMoving = true;
                GameManager.getInstance().HighlightTilesAt(gridPosition, Color.cyan, movementPerActionPoint);
            } else
            {
                isMoving = false;
                GameManager.getInstance().RemoveTileHighlights();
            }
            isAttacking = false;
        }


        //attack button
        rectButton = new Rect(0, Screen.height - buttonHeight * 2, buttonWidth, buttonHeight);

        if (GUI.Button(rectButton, "Attack"))
        {
            if(!isAttacking)
            {
                GameManager.getInstance().RemoveTileHighlights();
                isAttacking = true;
                GameManager.getInstance().HighlightTilesAt(gridPosition, Color.yellow, attackRange);
            } else
            {
                isAttacking = false;
                GameManager.getInstance().RemoveTileHighlights();
            }
            isMoving = false;
        }


        //end turn
        rectButton = new Rect(0, Screen.height - buttonHeight * 1, buttonWidth, buttonHeight);

        if(GUI.Button(rectButton, "End Turn"))
        {
            GameManager.getInstance().RemoveTileHighlights();
            numberOfActionsPerTurn = 2;
            isMoving = false;
            isAttacking = false;
            GameManager.getInstance().NextTurn();
        }

        base.TurnOnGUI();
    }
}
