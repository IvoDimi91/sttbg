﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileHighlight {

    public TileHighlight()
    {

    }

    public static List<Tile> FindHighlight(Tile originTile, int movementPoints)
    {
        List<Tile> visited = new List<Tile>();

        List<TilePath> unvisited = new List<TilePath>();

        TilePath originPath = new TilePath();
        originPath.AddTileToPath(originTile);

        unvisited.Add(originPath);

        while(unvisited.Count > 0)
        {
            TilePath currentTilePath = unvisited[0];
            unvisited.Remove(unvisited[0]);
            
            if(visited.Contains(currentTilePath.lastTile))
            {
                continue;
            }

            if (currentTilePath.costOfPath > movementPoints + 1)
            {
                continue;
            }

            visited.Add(currentTilePath.lastTile);

            foreach (Tile t in currentTilePath.lastTile.neighbors)
            {
                //int distanceBetween = t.movingCost + currentTilePath.getCostOfPath();
                if (t.isImpassible)
                {
                    continue;
                }
                TilePath newTilePath = new TilePath(currentTilePath);
                newTilePath.AddTileToPath(t);
                unvisited.Add(newTilePath);


            }

        }
        visited.Remove(originTile);
        return visited;
    }
}
