﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

    public Vector2 gridPosition = Vector2.zero;

    // HANDLE DIFFERENT TERRAIN BY SETTING A BIGGER NUMBER !
    public int movingCost = 1;
    public bool isImpassible = false;
    public List<Tile> neighbors;

	// Use this for initialization
	void Start () {
        GenerateNeighbors();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnMouseEnter()
    {
        /*if (GameManager.getInstance().players[GameManager.getInstance().currentPlayerIndex].isMoving)
        {
            transform.GetComponent<Renderer>().material.color = Color.blue;
        } else if (GameManager.getInstance().players[GameManager.getInstance().currentPlayerIndex].isAttacking)
        {
            transform.GetComponent<Renderer>().material.color = Color.red;
        }*/
        //Debug.Log("My Position " + gridPosition.x + " ; " + gridPosition.y);
    }

    private void OnMouseExit()
    {
        //transform.GetComponent<Renderer>().material.color = Color.white;
    }


    // !!! transform.GetComponent<Renderer>().material CAN ADD ANOTHER MATERIAL FOR IMPASSIBLE !!!

    private void OnMouseDown()
    {
        if (GameManager.getInstance().players[GameManager.getInstance().currentPlayerIndex].isMoving)
        {
            GameManager.getInstance().MoveCurrentPlayer(this);
        }
        else if (GameManager.getInstance().players[GameManager.getInstance().currentPlayerIndex].isAttacking)
        {
            GameManager.getInstance().AttackWithCurrentPlayer(this);
        } else {
            isImpassible = !isImpassible;
            if (isImpassible)
            {
                transform.GetComponent<Renderer>().material.color = Color.gray;
            }
            else
            {
                transform.GetComponent<Renderer>().material.color = Color.white;
            }
        }
    }

    private void GenerateNeighbors()
    {
        neighbors = new List<Tile>();
        //up
        if (gridPosition.y > 0)
        {
            Vector2 n = new Vector2(gridPosition.x, gridPosition.y - 1);
            neighbors.Add(GameManager.getInstance().getMap()[(int) Mathf.Round(n.x)][(int)Mathf.Round(n.y)]);
        }
        //down
        if (gridPosition.y < GameManager.getInstance().getMap().Count - 1)
        {
            Vector2 n = new Vector2(gridPosition.x, gridPosition.y + 1);
            neighbors.Add(GameManager.getInstance().getMap()[(int)Mathf.Round(n.x)][(int)Mathf.Round(n.y)]);
        }
        //left
        if (gridPosition.x > 0)
        {
            Vector2 n = new Vector2(gridPosition.x - 1, gridPosition.y);
            neighbors.Add(GameManager.getInstance().getMap()[(int)Mathf.Round(n.x)][(int)Mathf.Round(n.y)]);
        }
        //right
        if (gridPosition.x < GameManager.getInstance().getMap().Count - 1)
        {
            Vector2 n = new Vector2(gridPosition.x + 1, gridPosition.y);
            neighbors.Add(GameManager.getInstance().getMap()[(int)Mathf.Round(n.x)][(int)Mathf.Round(n.y)]);
        }
    }
}
