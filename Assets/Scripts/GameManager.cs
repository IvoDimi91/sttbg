﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private static GameManager instance;

    public GameObject tilePrefab;
    public int mapSize = 11;

    public GameObject humanPlayerPrefab;
    public GameObject aiPlayerPrefab;

    List<List<Tile>> map = new List<List<Tile>>();

    [HideInInspector]
    public List<Player> players;
    public int currentPlayerIndex = 0;

    public List<List<Tile>> getMap()
    {
        return map;
    }

    public static GameManager getInstance()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        GenerateMap();
        GeneratePlayers();
	}
	
	// Update is called once per frame
	void Update () {
        if(players[currentPlayerIndex].HP > 0)
        {
            players[currentPlayerIndex].TurnUpdate();
        } else
        {
            NextTurn();
        }
        
	}

    private void OnGUI()
    {
        if (players[currentPlayerIndex].HP > 0)
        {
            players[currentPlayerIndex].TurnOnGUI();
        }
    }

    public void NextTurn()
    {
        if(currentPlayerIndex + 1 < players.Count)
        {
            currentPlayerIndex++;
        } else
        {
            currentPlayerIndex = 0;
        }
    }

    public void HighlightTilesAt(Vector2 originLocation, Color highlightColor, int pointsPerAction)
    {
        List<Tile> highlightedTiles = TileHighlight.FindHighlight(map[(int)originLocation.x][(int)originLocation.y], pointsPerAction);

        foreach(Tile t in highlightedTiles)
        {
            t.transform.GetComponent<Renderer>().material.color = highlightColor;
        }
    }

    public void RemoveTileHighlights()
    {
        for(int i = 0 ; i < mapSize; i++)
        {
            for(int j = 0; j < mapSize; j++)
            {
                if(!map[i][j].isImpassible)
                {
                    map[i][j].transform.GetComponent<Renderer>().material.color = Color.white;
                }
            }
        }
    }

    public void MoveCurrentPlayer(Tile destTile)
    {
        if (destTile.transform.GetComponent<Renderer>().material.color != Color.white && !destTile.isImpassible)
        {
            RemoveTileHighlights();
            players[currentPlayerIndex].isMoving = false;
            //players[currentPlayerIndex].moveDestination = destTile.transform.position + 1.5f * Vector3.up;
            List<Tile> path = TilePathFinder.FindPath(map[(int)players[currentPlayerIndex].gridPosition.x][(int)players[currentPlayerIndex].gridPosition.y], destTile);

            foreach (Tile t in path)
            {
                players[currentPlayerIndex].positionQueue.Add(map[(int)t.gridPosition.x][(int)t.gridPosition.y].transform.position + 1.5f * Vector3.up);
                Debug.Log(players[currentPlayerIndex].positionQueue[players[currentPlayerIndex].positionQueue.Count - 1]);
            }

            players[currentPlayerIndex].gridPosition = destTile.gridPosition;
        } else
        {
            Debug.Log("Invalid destination !");
        }
    }

    public void AttackWithCurrentPlayer(Tile destTile)
    {
        if (destTile.transform.GetComponent<Renderer>().material.color != Color.white && !destTile.isImpassible)
        {
            Player currentPlayer = players[currentPlayerIndex];
            Player target = null;
            foreach (Player p in players)
            {
                if (p.gridPosition == destTile.gridPosition)
                {
                    target = p;
                    //break;
                }
            }

            if (target != null)
            {
                //Debug.Log(currentPlayer.gridPosition + " | | | " + target.gridPosition);
                //attack logic
                if (currentPlayer.gridPosition.x >= target.gridPosition.x - 1
                    && currentPlayer.gridPosition.x <= target.gridPosition.x + 1
                    && currentPlayer.gridPosition.y >= target.gridPosition.y - 1
                    && currentPlayer.gridPosition.y <= target.gridPosition.y + 1)
                {

                    bool successfulHit = Random.Range(0.0f, 1.0f) <= currentPlayer.attackChance;

                    if (successfulHit)
                    {
                        //damage
                        int amountOfDamage = (int)Mathf.Floor(currentPlayer.baseDamage + Random.Range(0, currentPlayer.damageRollSide));
                        Debug.Log("Hit " + currentPlayer.playerName + " successfuly hit " + target.playerName + " for " + amountOfDamage + " damage");

                        target.HP -= amountOfDamage;
                    }
                    else
                    {
                        //miss
                        Debug.Log("Hit " + players[currentPlayerIndex].playerName + " MISSED " + target.playerName);
                    }
                    players[currentPlayerIndex].numberOfActionsPerTurn--;
                }
                else
                {
                    Debug.Log("Please, move towards enemy to attack !");
                }
            }
        } else
        {
            Debug.Log("Invalid ATTACK !");
        }
    }

    private void GenerateMap()
    {
        map = new List<List<Tile>>();
        for(int x=0; x < mapSize; x++)
        {
            List<Tile> row = new List<Tile>();
            for(int z=0; z < mapSize; z++)
            {
                Tile tile = ((GameObject) Instantiate(tilePrefab, new Vector3(x - Mathf.Floor(mapSize/2),0, -z + Mathf.Floor(mapSize/2)), Quaternion.Euler(new Vector3()))).GetComponent<Tile>();
                tile.gridPosition = new Vector2(x, z);
                row.Add(tile);
            }
            map.Add(row);

        }
    }

    private void GeneratePlayers()
    {
        players = new List<Player>();

        HumanPlayer player;

        AiPlayer aiPlayer;

        player = ((GameObject)Instantiate(humanPlayerPrefab, new Vector3(0 - Mathf.Floor(mapSize / 2), 1.5f, 0 + Mathf.Floor(mapSize / 2)), Quaternion.Euler(new Vector3()))).GetComponent<HumanPlayer>();
        player.gridPosition = new Vector2(0, 0);
        player.playerName = "Ivo";
        players.Add(player);

        player = ((GameObject)Instantiate(humanPlayerPrefab, new Vector3((mapSize-1) - Mathf.Floor(mapSize / 2), 1.5f, -(mapSize - 1) + Mathf.Floor(mapSize / 2)), Quaternion.Euler(new Vector3()))).GetComponent<HumanPlayer>();
        player.gridPosition = new Vector2((mapSize - 1), (mapSize - 1));
        player.playerName = "Ivan";
        players.Add(player);

        player = ((GameObject)Instantiate(humanPlayerPrefab, new Vector3(4 - Mathf.Floor(mapSize / 2), 1.5f, -4 + Mathf.Floor(mapSize / 2)), Quaternion.Euler(new Vector3()))).GetComponent<HumanPlayer>();
        player.gridPosition = new Vector2(4, 4);
        player.playerName = "Petkan";
        players.Add(player);

        //aiPlayer = ((GameObject)Instantiate(aiPlayerPrefab, new Vector3(6 - Mathf.Floor(mapSize / 2), 1.5f, -4 + Mathf.Floor(mapSize / 2)), Quaternion.Euler(new Vector3()))).GetComponent<AiPlayer>();

        //players.Add(aiPlayer);
    }
}
