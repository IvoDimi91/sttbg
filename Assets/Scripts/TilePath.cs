﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TilePath
{
    public List<Tile> listOfTiles = new List<Tile>();
    public int costOfPath = 0;
    public Tile lastTile;

    public TilePath()
    {

    }

    public TilePath(TilePath tilePath)
    {
        listOfTiles = tilePath.listOfTiles.ToList();
        costOfPath = tilePath.costOfPath;
        lastTile = tilePath.lastTile;
    }

    public void AddTileToPath(Tile tile)
    {
        costOfPath += tile.movingCost;
        listOfTiles.Add(tile);
        lastTile = tile;
    }

    /*public Tile GetLastTile()
    {
        if(listOfTiles.Count > 0)
        {
            return listOfTiles[listOfTiles.Count-1];
        }
        return null;
    }*/

    /*public int getCostOfPath()
    {
        return costOfPath;
    }*/
}